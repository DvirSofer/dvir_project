import Painter
import PySimpleGUI as sg
import threading
import os
import os.path

file_types = [("PNG (*.png)", "*.png"), ("JPEG (*.jpg)", "*.jpg")]

def main_window():
    laylot = [[sg.Text("Welcome to Motion Painter!!")],
              [sg.Button("Start Blank Paint Canvas"), sg.Button("Load image to paint"), sg.Button("Motion Guide")]]
    return sg.Window("Main_Window", laylot, finalize=True)

def second_window():
    laylot = [[sg.Text("*Image File", text_color='black'),
                sg.Input(size=(25, 1), key="-FILE-"),
                sg.FileBrowse(file_types=file_types, key="file_browser")],
                [sg.Checkbox("Save drawing", enable_events=True, key="-IN-")],
                [sg.Text("Folder(optional):", text_color="grey", key="folder_text"),
                 sg.Input(size=(22, 1), key="-FOLDER-", disabled=True),
                 sg.FolderBrowse(disabled=True, key="folder_browser")],
                [sg.Text("Drawing Name (Default is 'myDrawing'):", text_color="grey", key="name_text"),
                 sg.Input(size=(15, 1), key="-NAME-", disabled=True)],
                [sg.Button("Load Image")]]
    return sg.Window("Browse_Window", laylot, finalize=True)

def third_window():
    laylot = [[sg.Text("*Height(max 640, min 100):", text_color='black', key="height"),
               sg.InputText(size=(24, 1), key="-HEIGHT-")],
              [sg.Text("*Width(max 1080, min 100):", text_color='black', key="width"),
               sg.InputText(size=(24, 1), key="-WIDTH-")],
              [sg.Checkbox("Save drawing", enable_events=True, key="-IN-")],
              [sg.Text("Folder(optional):", text_color="grey", key="folder_text"),
               sg.Input(size=(22, 1), key="-FOLDER-", disabled=True),
               sg.FolderBrowse(disabled=True, key="folder_browser")],
              [sg.Text("Drawing Name (Default is 'myDrawing'):", text_color="grey", key="name_text"),
               sg.Input(size=(15, 1), key="-NAME-", disabled=True)],
              [sg.Button("Load Canvas")]]
    return sg.Window("Canvas Generator", laylot, finalize=True)

def create_b(window, height, width, folderPath, fileName, toSave):
    """
    function call the Painter file to create blank canvas.
    :param window: current gui window.
    :return: None
    """
    save = toSave
    if int(height) > 640:
        height = "640"
    if int(height) < 100:
        height = "100"
    if int(width) > 1080:
        width = "1080"
    if int(width) < 100:
        width = "100"
    if save:
        if folderPath == '' or os.path.isdir(folderPath) is False:
            save = False
        if fileName == '':
            fileName = "myDrawing"
    p = Painter.Painter(window, save, folderPath, fileName)
    p.create_blank(height, width)

def open_p(window, fileName, folderPath, imageName, toSave):
    """
    function call the Painter file to create image based canvas.
    :param window: current gui window
    :param fileName: image path
    :return:
    """
    save = toSave
    if toSave:
        if folderPath == '' or os.path.isdir(folderPath) is False:
            save = False
        if imageName == '':
            imageName = "myDrawing"
    p = Painter.Painter(window, save, folderPath, imageName)
    p.open_pic(fileName)

def main():

    window1, window2, window3 = main_window(), None, None
    save = False
    while True:
        window, event, values = sg.read_all_windows()
        if event == sg.WIN_CLOSED or event == 'Exit':
            window.close()
            if window == window2:       # if closing win 2, mark as closed
                window2 = None
                window1.UnHide()
            elif window == window3:
                window3 = None
                window1.UnHide()
            elif window == window1:     # if closing win 1, exit program
                break
        elif event == "Load Canvas":   # if user wants blank canvas
            flag = True
            for i in values["-HEIGHT-"]:
                if ord(i) < 48 or ord(i) > 57:
                    flag = False
                    window.find_element("height").Update(text_color="red")
                    break
            for i in values["-WIDTH-"]:
                if flag and (ord(i) < 48 or ord(i) > 57):
                    flag = False
                    window.find_element("width").Update(text_color="red")
                    break

            if values["-HEIGHT-"] == '' or flag == False:
                    window.find_element("height").Update(text_color="red")
            else:
                window.find_element("height").Update(text_color="black")
            if values["-WIDTH-"] == '' or flag == False:
                window.find_element("width").Update(text_color="red")
            else:
                window.find_element("width").Update(text_color="black")

            if values["-HEIGHT-"] != '' and values["-WIDTH-"] != '' and flag:
                window.find_element("height").Update(text_color="black")
                window.find_element("width").Update(text_color="black")
                t = threading.Thread(target=create_b, args=(window, values["-HEIGHT-"], values["-WIDTH-"],
                                                            values["-FOLDER-"], values["-NAME-"], save))
                window.Hide()
                t.start()
                t.join()
                window.UnHide()
        elif event == "Start Blank Paint Canvas":
            window3 = third_window()
            window1.Hide()
        elif event == "Load image to paint":    # if user wants base image
            window2 = second_window()
            window1.Hide()
        elif event == "Load Image": # loading image for the user
            if values["-FILE-"]:
                fileName = values["-FILE-"]
                if os.path.exists(fileName):
                    t = threading.Thread(target=open_p, args=(window, fileName, values["-FOLDER-"],
                                                              values["-NAME-"], save))
                    window.Hide()
                    t.start()
                    t.join()
                    window.UnHide()
            else:
                window.find_element("file_browser").Update(button_color="red")
        elif event == "-IN-":
            if values["-IN-"] == True:
                window.find_element("folder_text").Update(text_color="black")
                window.find_element("folder_browser").Update(disabled=False)
                window.find_element("name_text").Update(text_color="black")
                window.find_element("-NAME-").Update(disabled=False)
                save = True
            else:
                window.find_element("folder_text").Update(text_color="grey")
                window.find_element("folder_browser").Update(disabled=True)
                window.find_element("name_text").Update(text_color="grey")
                window.find_element("-NAME-").Update(disabled=True)
                save = False
        elif event == "Motion Guide":
            os.startfile("painter guide.docx")

if __name__ == "__main__":
    main()
