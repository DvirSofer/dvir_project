import threading
import tkinter
from tkinter import *
import Camera
import PySimpleGUI
from PIL import ImageGrab
import os.path
from os import path

class Painter:
    def __init__(self, main_window, save, folderPath, fileName):
        """
        the Painter constractor.
        :param main_window: current gui window.
        """
        master = tkinter.Tk()
        master.resizable(False, False)
        self.master = master
        self.main_window = main_window
        self.camera = Camera.Camera()
        self.toSave = save
        self.folderPath = folderPath
        self.fileName = fileName
        self.currX = 10
        self.currY = 5
        self.conCurrX = 0.99
        self.conCurrY = 0.01
        self.colorStorage = ["black", "red", "purple", "yellow", "green", "blue", "brown", "white", "pink"]
        self.colorIndex = 0
        self.radius = 5
        self.toContinue = True
        self.leftActiv = True
        self.currentPoint = None
        self.controlPoint = None
        self.Canvas = None

    def save(self):
        endPath = self.folderPath + "\\" + self.fileName + ".png"
        x = self.master.winfo_rootx() + self.Canvas.winfo_x()
        y = self.master.winfo_rooty() + self.Canvas.winfo_y()
        x1 = x + self.Canvas.winfo_width()
        y1 = y + self.Canvas.winfo_height()
        while path.exists(endPath):
            endPath = endPath[:len(endPath) - 4]
            endPath += '0'
            endPath += ".png"
        ImageGrab.grab().crop((x,y,x1,y1)).save(endPath)

    def quit(self):
        """
        stop the canvas drawing and the camera
        :return: None
        """
        if self.toSave:
            self.save()
        self.master.destroy()
        self.camera.stop()

    def check_size(self, img):
        """
        check the image size and makes it smaller if needed.
        :param img: the image that need to be checked.
        :return: the image after checking and resizing if needed.
        """
        while img.width() > 1280 or img.height() > 720:
            img = img.subsample(2, 2)
        return img

    def center_window(self, height, width):
        """
        place the canvas in the center of the screen.
        :param width: canvas width.
        :param height: canvas height.
        :return: None.
        """
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()
        x = (screen_width/2) - (width/2)
        y = (screen_height/2) - (height/2)
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y - 37))

    def deletePoint(self, point):
        if point:
            self.Canvas.delete(point)

    def redraw(self, can):
        self.currX, self.currY, self.colorIndex, self.radius, self.toContinue, self.leftActiv = self.camera.getCo()
        if self.toContinue:
            self.currX = can.winfo_width() - self.currX * can.winfo_width()
            self.currY = self.currY * can.winfo_height()
            self.currentPoint = can.create_oval(self.currX - self.radius, self.currY - self.radius,
                                                self.currX + self.radius, self.currY + self.radius,
                                                fill=self.colorStorage[self.colorIndex],
                                                outline=self.colorStorage[self.colorIndex])
            if self.leftActiv:
                outline = 0
                if self.colorIndex == 0:
                    outline = 7
                tConCurrX = can.winfo_width() - self.conCurrX * can.winfo_width()
                tConCurrY = self.conCurrY * can.winfo_height()
                self.controlPoint = can.create_oval(tConCurrX - self.radius, tConCurrY - self.radius,
                                                tConCurrX + self.radius, tConCurrY + self.radius,
                                                fill=self.colorStorage[self.colorIndex],
                                                outline=self.colorStorage[outline])
                can.after(1, self.deletePoint, self.currentPoint)
                can.after(1, self.deletePoint, self.controlPoint)
            can.after(1, self.redraw, can)
        else:
            self.deletePoint(self.controlPoint)
            self.quit()

    def create_blank(self, height, width):
        """
        creates blank canvas.
        :return: None.
        """
        self.center_window(int(height), int(width))
        self.Canvas = Canvas(self.master, height=int(height), width=int(width))
        self.Canvas.pack()
        self.master.protocol("WM_DELETE_WINDOW", self.quit)
        self.camera = self.camera.start()
        self.redraw(self.Canvas)
        self.master.mainloop()

    def open_pic(self, path):
        """
        creates image based canvas.
        :param path: image path.
        :return: None.
        """
        img = self.check_size(PhotoImage(file = path, master=self.master))
        self.center_window(img.height(), img.width() - 10)
        self.Canvas = Canvas(self.master, width=img.width(), height=img.height())
        self.Canvas.pack()
        self.Canvas.create_image(0, 0, anchor=NW, image=img)
        self.master.protocol("WM_DELETE_WINDOW", self.quit)
        self.camera = self.camera.start()
        self.redraw(self.Canvas)
        self.master.mainloop()
