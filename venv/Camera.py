import cv2
from threading import Thread
import mediapipe as mp

currX = 0
currY = 0
color = 0
radius = 5
toContinue = True
leftActiv = True

class Camera:
    def __init__(self):
        """
        the Camera constractor.
        """
        self.cap = cv2.VideoCapture(0)
        (self.ret, self.frame) = self.cap.read()
        self.stopped = False
        self.thr = Thread(target=self.get, args=())
    
    def getCo(self):
        global currX
        global currY
        global color
        global radius
        global toContinue
        global leftActiv
        return currX, currY, color, radius, toContinue, leftActiv
    
    def start(self):
        global toContinue
        """
        starts the thread of the camera input.
        :return: self.
        """
        toContinue = True
        self.thr.start()
        return self

    def get(self):
        """
        gets input from the camera.
        :return: None.
        """
        global currX
        global currY
        global color
        global radius
        global toContinue
        global leftActiv
        flag = 0
        mp_hands = mp.solutions.hands
        with mp_hands.Hands(min_detection_confidence=0.5,
                            min_tracking_confidence=0.5,
                            max_num_hands=2) as hands:
            while not self.stopped:
                if not self.ret:
                    self.stop()
                else:
                    (self.ret, self.frame) = self.cap.read()
                    self.frame.flags.writeable = False
                    self.frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
                    results = hands.process(self.frame)
                    if results.multi_hand_landmarks:
                        if len(results.multi_hand_landmarks) == 2:
                            leftActiv = True
                            currX = results.multi_hand_landmarks[0].landmark[8].x
                            currY = results.multi_hand_landmarks[0].landmark[8].y
                        else:
                            for hand in results.multi_hand_landmarks:
                                if hand.landmark[4].x >  hand.landmark[20].x:
                                    leftActiv = False
                                    currX = hand.landmark[8].x
                                    currY = hand.landmark[8].y
                                    flag = 0
                                else:
                                    leftActiv = True
                                    currX = 1000
                                    currY = 1000
                                    if hand.landmark[4].x > hand.landmark[1].x:
                                        if hand.landmark[8].y > hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            color = 0
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            color = 1
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y < hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            color = 2
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y < hand.landmark[10].y and hand.landmark[16].y < hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            color = 3
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y < hand.landmark[10].y and hand.landmark[16].y < hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            color = 4
                                        elif hand.landmark[8].y > hand.landmark[6].y and hand.landmark[12].y < hand.landmark[10].y and hand.landmark[16].y < hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            color = 5
                                        elif hand.landmark[8].y > hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y < hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            color = 6
                                        elif hand.landmark[8].y > hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            color = 7
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            color = 8
                                        flag = 0
                                    else:
                                        if hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            if radius < 30 and flag == 0:
                                                radius += 5
                                                flag = 1
                                        elif hand.landmark[8].y < hand.landmark[6].y and hand.landmark[12].y < hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y > hand.landmark[17].y:
                                            if radius > 5 and flag == 0:
                                                radius -= 5
                                                flag = 1
                                        elif hand.landmark[8].y > hand.landmark[6].y and hand.landmark[12].y > hand.landmark[10].y and hand.landmark[16].y > hand.landmark[14].y and hand.landmark[20].y < hand.landmark[17].y:
                                            toContinue = False
                                        else:
                                            flag = 0
                    else:
                        continue

    def stop(self):
        """
        stop the Camera thread.
        :return: None.
        """
        global currX
        global currY
        global color
        global  radius
        global leftActiv
        color = 0
        conCurrX = 0
        conCurrY = 0
        radius = 5
        leftActiv = True
        self.stopped = True
